import React from "react";
import ReactDOM from "react-dom/client";
// import { Provider } from "react-redux";
// import store from "./store";
// import "./index.css";
// import "mdb-react-ui-kit/dist/css/mdb.min.css";
import App from "./App";
// import Ws from "/src/WebSocket/Ws.jsx";
// import reportWebVitals from "./reportWebVitals";

// import "./styles/normalize.css";
// import "./styles/fonts.css";
// import "./styles/base.pcss";
// import "./styles/animation.pcss";
// import "./styles/typography.pcss";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  // <React.StrictMode>
  // <Provider store={store}>
  <App />
  //  <Ws />
  // </Provider>
  // </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
