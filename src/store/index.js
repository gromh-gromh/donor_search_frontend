import { configureStore } from "@reduxjs/toolkit";
import mainSlice from "/src/store/mainSlice/mainSlice.js";

export default configureStore({
  reducer: {
    mainSlice: mainSlice,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({ serializableCheck: false }),
});
