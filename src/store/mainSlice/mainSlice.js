import { createSlice } from "@reduxjs/toolkit";

var mainSlice = createSlice({
  name: "main",
  initialState: {
    field: "",
  },
  reducers: {
    setField(state, action) {
      state.field = action.payload.value;
    },
  },
});

export var { setField } = mainSlice.actions;

export default mainSlice.reducer;
