import { BrowserRouter, Route, Routes } from "react-router-dom";
import "/src/App.pcss";
import React, { useState, useEffect } from "react";
import Main from "/src/components/Main/Main.jsx";
// import Constructor from "/src/components/Constructor/Constructor.jsx";
// import TestComponent from "/src/components/TestComponent/TestComponent.jsx";
// import { useSelector } from "react-redux";
// import Icon from "/src/components/Icon/Icon.jsx";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="*" element={<Main />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
